/**
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, ViewPropTypes} from 'react-native';

/*设置属性值*/
type Props = {
    title?: string,/*标题可设置也可不设置*/
    titleStyle?: ViewPropTypes.style,
    onPress?: Function,
    icon?: any,
    iconStyle?: ViewPropTypes.style,

};
type State = {};
export default class NavigationItem extends PureComponent<Props, State> {
    render() {
        let {title, titleStyle, onPress, icon, iconStyle} = this.props/*取得属性值*/
        let titleElement = title && (/*title 不为null 才能使用该参数"&&"*/
            <Text style={[styles.title, titleStyle]}>{title}</Text>
        )
        let iconElement = icon && (
            <Image source={icon} style={[styles.icon, iconStyle]}/>
        )

        return (
            <TouchableOpacity onPress={onPress} style={styles.container}>
                {titleElement}
                {iconElement}
            </TouchableOpacity>
        );
    }
}

/*顶部导航样式*/
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 15,
        color: '#333333',
        margin: 8,

    },
    icon: {
        width: 27,
        height: 27,
        margin: 8,
    }


});


