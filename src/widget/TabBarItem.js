/**
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';


type Props = {
    normalImage: any,
    selectedImage: any,
    focused: boolean,
    tintColor: any,
};
type State = {};
export default class TabBarItem extends PureComponent<Props, State> {
    render() {
        let {normalImage, selectedImage, focused, tintColor} = this.props
        return (
            <Image
                source={focused ? selectedImage : normalImage}
                style={{width: 25, height: 25, tintColor: tintColor}}></Image>

        );
    }
}


const styles = StyleSheet.create({});


