/**
 * @format
 * @flow
 * 分割线重构
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import color from "./color";


type Props = {};
type State = {};
export default class SpacingView extends PureComponent<Props, State> {
    render() {
        return (
            /*灰色分割线*/
            <View style={styles.spcing}/>
        )
    }
}


const styles = StyleSheet.create({
    spcing: {
        height: 14,
        backgroundColor: color.paper
    }
});


