/**
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image, InteractionManager} from 'react-native';
import {WebView} from 'react-native-webview'


type Props = {};
type State = {};
export default class WebScene extends PureComponent<Props, State> {
    /*对象构造方法*/
    constructor(props: Props) {
        super(props)
        // alert('url:' + this.props.navigation.state.params.url)/*取出HomeScene 网格点击事件传来的url*/

    }

    /*挂载UI时自动执行方法，并执行render（）*/
    componentDidMount(): void {
        /*交互管理 运行完后执行 方法体*/
        InteractionManager.runAfterInteractions(() => {
            /*默认值设置为加载中*/
            this.props.navigation.setParams({title: '加载中'})
        })
    }

    static navigationOptions = ({navigation}) => ({
        /*取出设置好的标题值*/
        title: navigation.state.params.title


    })

    /*web加载完成后执行 e为事件参数*/
    onLoadEnd = (e) => {
        /*取得title值*/
        let title = e.nativeEvent.title
        if (title.length > 0) {
            /*{title:title} 键和值同名可忽略键，最终显示网页标题*/
            this.props.navigation.setParams({title})
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <WebView
                    style={styles.webView}
                    source={{uri: this.props.navigation.state.params.url}}
                    onLoadEnd={this.onLoadEnd}
                />

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    webView: {
        flex: 1
    }
});


