/**
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions} from 'react-native';
import color from '../../widget/color'
import NavigationItem from '../../widget/NavigationItem'
import HomeMenuView from '../Home/HomeMenuView'
import * as api from '../../repository/api'
import {discount} from "../../repository/api";
import HomeGridView from './HomeGridView'
import SpacingView from '../../widget/SpacingView'
import {Heading2, Heading3} from '../../widget/Text'
/*属性*/
type Props = {};
/*params传输状态*/
type State = {
    discounts: Array<Object>
};
export default class HomeScene extends PureComponent<Props, State> {
    /*createStackNavigator 设置导航选项*/
    static navigationOptions = () => ({
        /*顶部背景颜色：绿色*/
        headerStyle: {backgroundColor: color.primary},
        /*搜索栏标题*/
        headerTitle: (
            /*可点击容器包裹*/
            <TouchableOpacity style={styles.searchBar}/*搜索栏样式*/>
                <Image source={require('../../img/home/search_icon.png')/*搜索图标*/} style={styles.searchIcon}/*搜索图标样式*//>
                <Text style={{fontSize: 14}/*搜索字体大小*/}>搜索</Text>
            </TouchableOpacity>
        ),
        /*左侧按钮*/
        headerLeft: (
            /*自定义导航项class*/
            <NavigationItem
                title='定位'/*有值用，无值自带默认值*/
                titleStyle={{color: 'white'}}/*有值用，无值自带默认值**/
                onPress={() => {/*Toast*/
                    alert('test')
                }}/>
        ),
        /*右侧按钮*/
        headerRight: (
            /*自定义导航项class*/
            <NavigationItem
                icon={require('../../img/mine/icon_navigationItem_message_white.png')}/*铃铛图标*/
                onPress={() => {

                }}
            />
        )


    })

    constructor(props) {
        super(props);
        this.state = {
            discounts: []
        }
    }

    /*ui挂载完成后调用请求数据*/
    componentDidMount() {
        this.requestData()
    }


    /*网络请求网格数据,返回给discounts*/
    requestData = async () => {
        try {
            /*网络请求代码*/
            /*// let response = await fetch(api.discount)
            // let json = await response.json()
            // this.setState({discounts: json.data})
            // alert("test:" + JSON.stringify(json.data))*/

            /*本地写死的代码（模拟请求数据）*/
            let json = api.discount
            this.setState({discounts: json.data})
        } catch (e) {
            alert('error' + e)
        }
    }

    /*箭头函数目的：传递上下文*/
    onGridSelected = (index) => {
        let discount = this.state.discounts[index]
        /*取出url 并使用WEBVIEW渲染*/
        // alert('test:' + JSON.stringify(discount))
        if (discount.type == 1) {
            let location = discount.tplurl.indexOf('http')
            let url = discount.tplurl.slice(location)
            /*页面跳转并传递参数URL*/
            this.props.navigation.navigate('WebScene', {url: url})

        }
    }


    render() {
        return (
            <View style={{flex: 1}}>
                {/*<TouchableOpacity onPress={() => {*/}
                {/*// alert('test')*/}
                {/*this.props.navigation.navigate('WebScene' ,{url:'http://www.baidu.com'})*/}
                {/* </TouchableOpacity>*/}
                {/*}}>*/}
                {/* <Text style={{fontSize: 30}}>Home!</Text>*/}

                {/*首页团购：17个按钮*/}
                <HomeMenuView
                    menuInfos={api.menuInfos}
                    onMenuSelected={(index) => {
                        alert('test:' + index)
                    }}/>

                {/*灰色分割线*/}
                <SpacingView/>

                {/*网格容器 展示UI网格 和点击事件*/}
                <HomeGridView infos={this.state.discounts} onGridSelected={this.onGridSelected}/>

                <SpacingView/>


            </View>
        );
    }
}


const styles = StyleSheet.create({
    /*搜索框样式*/
    searchBar: {
        flexDirection: 'row',/*从左到右*/
        width: Dimensions.get('window').width * 0.7,/*宽度70%*/
        height: 30,/*高度*/
        borderRadius: 19,/*圆角*/
        justifyContent: 'center',/*主轴居中*/
        alignItems: 'center',/*纵轴居中*/
        backgroundColor: 'white'/*背景白色*/
    },
    /*搜索框图标*/
    searchIcon: {
        width: 20,
        height: 20,
        margin: 5/*外边距*/
    },


});


