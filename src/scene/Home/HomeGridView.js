/**
 * @format
 * @flow
 * 网格容器 重构
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import color from "../../widget/color";
import HomeGridItem from './HomeGridItem'


type Props = {
    infos: Array<Object>,
    onGridSelected: Function,
};
type State = {};
export default class HomeGridView extends PureComponent<Props, State> {
    render() {
        let {infos, onGridSelected} = this.props
        return (
            <View style={styles.container}>
                {infos.map((info, index) => (
                    <HomeGridItem
                        key={index}
                        info={info}
                        /*一对多关系，传人索引*/
                        onPress={() => {
                            onGridSelected(index)
                        }}

                    />
                ))}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderColor: color.border,

    }
});


