/**
 * @format
 * @flow
 * 17个按钮外部容器设定
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import HomeMenuItem from './HomeMenuItem'
import screen from '../../common/screen'
import PageControl from 'react-native-page-control'
import color from '../../widget/color'


type
Props = {
    menuInfos: Array < Object >,
    onMenuSelected: Function

};
type
State = {
    currentPage: number,
};
export default class HomeMenuView extends PureComponent<Props, State> {
    constructor(props: Object) {
        super(props)
        /*设置初始状态：0*/
        this.state = {
            currentPage: 0,
        }
    }

    /*取出滚动视图的偏移量*/
    onScroll = (e) => {
        /*X轴滑动的偏移量*/
        let x = e.nativeEvent.contentOffset.x
        /*4舍5入运算偏移量，生成当前页码*/
        let currentPage = Math.round(x / screen.width)
        /*设置当前的页码为滚动的页码，执行：setState 后 自动执行：render()方法渲染UI*/
        if (this.state.currentPage != currentPage) {/*之前页码不等于现在页码，才刷新*/
            this.setState({currentPage: currentPage})
        }

    }

    render() {
        /*取出每一个对象返回对应的菜单视图*/
        let {menuInfos, onMenuSelected} = this.props
        /*返回对应Item ui 数据 并且传递到HomeMenuItem中*/
        let menuElements = menuInfos.map((info, index) => (
            <HomeMenuItem
                key={index}
                title={info.title}
                icon={info.icon}
                onPress={() => {
                    onMenuSelected(index)/*点击事件返回到HomeScene的箭头函数中*/
                }}/>


        ))


        let pageCount = Math.ceil(menuInfos.length / 10)

        let menuViews = []

        /*每一页菜单数组*/
        for (let i = 0; i < pageCount; i++) {
            let elementPerPage = menuElements.slice(i * 10, i * 10 + 10)

            /*返回菜单视图*/
            let menuView = (
                <View key={i} style={styles.itemsView}>
                    {elementPerPage}{/*设置数据到ui上，并分割成每页*/}
                </View>
            )
            /*将menuView放入menuViews数组中去*/
            menuViews.push(menuView)
        }


        return (
            /*白色背景*/
            <View style={styles.container}>
                {/*水平滚动ViewPager*/}
                <ScrollView
                    horizontal /*水平方向*/
                    pagingEnabled /*page可用*/
                    showsHorizontalScrollIndicator={false}/*滚动条不显示*/
                    onScroll={this.onScroll}
                >
                    {menuViews}
                </ScrollView>
                <PageControl
                    style={styles.pageControl}
                    numberOfPages={pageCount} /*页码*/
                    currentPage={this.state.currentPage}/*当前页数*/
                    pageIndicatorTintColor={'gray'} /*未选中状态页码颜色*/
                    currentPageIndicatorTintColor={color.primary} /*已选中状态页码颜色*/


                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    /*背景白色*/
    container: {
        backgroundColor: 'white',
    },
    /*菜单排列方式*/
    itemsView: {
        flexDirection: 'row',/*水平方向*/
        flexWrap: 'wrap',/*自适应换行*/
        width: screen.width,
    },
    pageControl: {
        margin: 10
    }
});


