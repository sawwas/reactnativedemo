/**
 * @format
 * @flow
 * 17个按钮Item UI和数据绑定
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, Dimensions} from 'react-native';
import screen from '../../common/screen'


type
    Props = {
    title: string,
    icon: any,
    onPress: Function,

};
type
    State = {};
export default class HomeMenuItem extends PureComponent<Props, State> {
    render() {
        /*接收HomeMenuView 数据*/
        let {title, icon, onPress} = this.props
        return (
            /*可点击：数据绑定Text 和 Image 和点击事件*/
            <TouchableOpacity style={styles.appContainer} onPress={onPress}>
                <Image source={icon} style={styles.icon}/>
                <Text>{title}</Text>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    appContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width / 5,/*设置Item容器大小*/
        height: screen.width / 5,
    },
    icon: {
        width: screen.width / 9, /*设置Item Image大小*/
        height: screen.width / 9,
        margin: 5
    }
});


