/**
 * @format
 * @flow
 * 吃喝玩乐网格UI和数据绑定
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import screen from '../../common/screen'
import color from '../../widget/color'
import {Heading2, Heading3} from '../../widget/Text'


type Props = {
    info: Object,
    onPress: Function,
};
type State = {};
export default class HomeGridItem extends PureComponent<Props, State> {
    render() {
        /*取出变量：主标题、 主标题颜色、副标题、图片地址*/
        let {info, onPress} = this.props
        let title = info.maintitle
        let color = info.typeface_color
        let deputycolor = info.deputy_typeface_color
        let subtitle = info.deputytitle
        let imageUrl = info.imageurl.replace('w.h', '120.0')
        return (
            /*返回网格UI渲染图*/
            <TouchableOpacity style={styles.container} onPress={onPress}>
                <View>
                    {/*重构Heading*/}
                    <Heading2 style={{color: color, marginBottom: 10}}>{title}</Heading2>
                    <Heading3 style={{color: deputycolor}}>{subtitle}</Heading3>
                </View>
                <Image style={styles.icon} source={{uri: imageUrl}}/>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width / 2 - StyleSheet.hairlineWidth,
        height: screen.width / 4,
        backgroundColor: 'white',
        borderRightWidth: StyleSheet.hairlineWidth,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: color.border,

    },
    icon: {
        width: screen.width / 5,
        height: screen.width / 5,
        // backgroundColor: 'blue',
        marginLeft: 10


    },
});


