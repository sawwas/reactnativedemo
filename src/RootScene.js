/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet, View, Image, Text} from 'react-native'/*引入widget*/
import {
    createBottomTabNavigator,/*创建底部导航*/
    TabNavigator,
    createAppContainer,/*创建APP容器*/
    TabBarBottom,/*创建底部按钮样式*/
    createStackNavigator/*创建顶部导航*/
} from 'react-navigation'
import HomeScene from './scene/Home/HomeScene'/*团购*/
import NearbyScene from './scene/Nearby/NearbyScene'/*附近*/
import OrderScene from './scene/Order/OrderScene'/*订单*/
import MineScene from './scene/Mine/MineScene'/*我的*/
import WebScene from './scene/Web/WebScene'/*网页面*/
import TabBarItem from './widget/TabBarItem'/*底部按钮点击样式*/
import color from './widget/color'
/*启动方式*/
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class RootScene extends PureComponent<{}> {
    render() {
        return (
            /*返回APP容器内容*/
            <AppContainer/>

        );
    }
}
/*底部导航和对应页面绑定*/
const Tab = createBottomTabNavigator(
    {
        Home: {
            screen: createStackNavigator({Home: HomeScene}),
            navigationOptions: () => ({
                tabBarLabel: "团购",
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem normalImage={require('./img/tabbar/tabbar_homepage.png')}
                                selectedImage={require('./img/tabbar/tabbar_homepage_selected.png')}
                                focused={focused}
                                tintColor={tintColor}/>
                    /*<Image
                        source={require('./img/tabbar/tabbar_homepage.png')}
                    />*/
                )

            })
        },
        Nearby: {
            screen: createStackNavigator({Nearby: NearbyScene}),
            navigationOptions: () => ({
                tabBarLabel: "附近",
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem normalImage={require('./img/tabbar/tabbar_merchant.png')}
                                selectedImage={require('./img/tabbar/tabbar_merchant_selected.png')}
                                focused={focused}
                                tintColor={tintColor}/>

                    /*<Image
                        source={require('./img/tabbar/tabbar_merchant.png')}
                    />*/
                )

            })
        },
        Order: {
            screen: createStackNavigator({Order: OrderScene}),
            navigationOptions: () => ({
                tabBarLabel: "订单",
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem normalImage={require('./img/tabbar/tabbar_order.png')}
                                selectedImage={require('./img/tabbar/tabbar_order_selected.png')}
                                focused={focused}
                                tintColor={tintColor}/>
                    /*<Image
                        source={require('./img/tabbar/tabbar_order.png')}
                    />*/
                )

            })
        },
        Mine: {
            screen: createStackNavigator({Mine: MineScene}),
            navigationOptions: () => ({
                tabBarLabel: "我的",
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem normalImage={require('./img/tabbar/tabbar_mine.png')}
                                selectedImage={require('./img/tabbar/tabbar_mine_selected.png')}
                                focused={focused}
                                tintColor={tintColor}/>
                    /*<Image
                        source={require('./img/tabbar/tabbar_mine.png')}
                    />*/
                )

            })
        }

    }, {/*4个按钮样式*/
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        lazy: true,
        animationEnabled: false,
        swipeEnable: false,
        tabBarOptions: {
            activeTintColor: color.primary,
            inactiveTintColor: color.gray,
            style: {backgroundColor: 'white'}
        }


    }
)
/*底部导航显示外层；不设置，由内层自由设置*/
Tab.navigationOptions = {
    header: null
}

/*创建顶部导航*/
const AppNavigator = createStackNavigator({
    Tab: {screen: Tab},/*包含底部导航*/
    WebScene: {screen: WebScene}/*包含跳转网页*/
}, {
    /*默认顶部导航样式*/
    defaultNavigationOptions: {
        headerBackTitle: null,/*返回按钮标题：null*/
        headerTintColor: '#333333'/*返回按钮颜色：黑色*/
    }
})
/*APP ui容器*/
const AppContainer = createAppContainer(AppNavigator)

const styles = StyleSheet.create({});
