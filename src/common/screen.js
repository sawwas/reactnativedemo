import {Dimensions} from 'react-native'
/*屏幕工具重构*/
export default {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}