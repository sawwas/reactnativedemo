/**
 * @format
 */

import {AppRegistry} from 'react-native';
import RootScene from './src/RootScene'; /*app入口*/
import {name as appName} from './app.json';/*app名称*/
/*BUG_URL:Get rid of “Remote debugger is in a background tab” warning in React Native
:https://stackoverflow.com/questions/41146446/get-rid-of-remote-debugger-is-in-a-background-tab-warning-in-react-native*/
import {YellowBox} from 'react-native';

YellowBox.ignoreWarnings(['Remote debugger']);

AppRegistry.registerComponent(appName, () => RootScene);/*Launch*/
